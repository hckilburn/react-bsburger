import React from 'react';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
import Backdrop from '../../UI/Backdrop/Backdrop';

const sideDrawer = (props) => {

    // conditionaly open and close the sidedrawer
    let attachedClasses = [classes.SideDrawer, classes.Close];
    if (props.opened) {
        attachedClasses = [classes.SideDrawer, classes.Open];
    }

    return (
        <>
            <Backdrop
                isAuthenticated={props.isAuth}
                show={props.opened}
                clicked={props.closed}></Backdrop>
            <div className={attachedClasses.join(' ')} onClick={props.closed}>
                <div className={classes.Logo}><Logo /></div>
                <nav><NavigationItems isAuthenticated={props.isAuth} /></nav>
            </div>
        </>
    );
}

export default sideDrawer;