import React from 'react';
import classes from './Spinner2.css';

const loader = () => (
    <> 
        <div className={classes.Loader}>Loading...</div>
    </>
);

export default loader;
