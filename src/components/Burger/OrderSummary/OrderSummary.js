import React, {Component} from 'react';
import Button from '../../UI/Button/Button'

class OrderSummary extends Component {

    // This could be a functional component it doesn't need to be a class.
    // componentWillUpdate() {
    //     // console.log("[OrderSummary] - in componentWillUpdate");
    // }

    render () {
        const ingredientSummary = Object.keys(this.props.ingredients)
        .map(igKey => {
            return (
                <li key={igKey}>
                    <span style={{textTransform: "capitalize"}}></span>{igKey} {this.props.ingredients[igKey]}
                </li>
            );
        });

        return (
            <>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: $ {this.props.price.toFixed(2)}</strong></p>
                <p>Continue to checkout?</p>
                <Button click={this.props.purchaseCancel} btnType={"Danger"}>CANCEL?</Button>
                <Button click={this.props.purchaseFinish} btnType={'Success'}>CONTINUE?</Button>
            </>
        );
    }
};

export default OrderSummary;