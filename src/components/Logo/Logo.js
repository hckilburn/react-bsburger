import React from 'react';
import classes from './Logo.css';
import burgerLogo from '../../assets/images/burger-logo.png';

const logo = (props) => (
    <> 
        <img className={classes.Logo} src={burgerLogo} alt="Yalls Big-Shit-Beefy Burger" />
    </>
);

export default logo;