import React from 'react';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import { connect } from 'react-redux';

import classes from './CheckoutSummary.css';

const checkoutSummary = (props) => {

    return (
        <div className={classes.CheckoutSummary}>
            <h1>Yum Yummmy BS Beefy Burger!</h1>
            <div style={{width: '100%', margin: 'auto'}} > 
                <Burger ingredients={props.ings} />
                <h4>For the low price of $ {props.price.toFixed(2)}</h4>
            </div>
            <Button btnType='Danger' click={props.onCheckoutCancelled} >CANCEL</Button>
            <Button btnType='Success' click={props.onCheckoutContinued}>CONTINUE</Button>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice
    };
};

export default connect(mapStateToProps)(checkoutSummary);