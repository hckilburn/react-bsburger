import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-burger-app-b3fc3.firebaseio.com/'
});

export default instance;