import React, {Component} from 'react';
import Modal from '../../components/UI/Modal/Modal';

//import classes from './Comp.css';

const withErrorHandler = (Wrappedacomponent, axios ) => {

    return class extends Component {

        state = {
            error: null
        }

        componentWillMount() {
            this.reqInterceptors = axios.interceptors.request.use(req => {
                this.setState({error: null});
                return req;
            });

            this.resInterceptors = axios.interceptors.response.use(res => res, error => {
                this.setState({error: error});
            });
       }

       componentWillUnmount() {
        //    console.log("withErrors - WillUunmount", this.reqInterceptors, this.resInterceptors);
            axios.interceptors.request.eject(this.reqInterceptors);
            axios.interceptors.response.eject(this.resInterceptors);
       }

       errorConfirmedHandler = () => {
            this.setState({error: null});
       }
        
        render() {

            return (
                <>
                    <Modal 
                        show={this.state.error}
                        modalClose={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <Wrappedacomponent {...this.props}/>
                </>
            );
        }
    }
}
export default withErrorHandler;