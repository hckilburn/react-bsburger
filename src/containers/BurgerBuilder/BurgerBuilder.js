import React, { Component } from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';

import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';


export class BurgerBuilder extends Component {
    state = {
        purchasing: false
    }

    componentDidMount() {
        // console.log("[BurgerBuilder] - in componentDidMount");
        // console.log(this.props);
        // if (!this.props.isAuthenticated || this.props.ings === null) {
            this.props.onInitIngredients();
        // }
    }

    // Purchasing (button order clicked)
        purchasingHandler = () => {
        // console.log("[BurgerBuilder] - in purchasingHandler");
        if (this.props.isAuthenticated) {
            this.setState({ purchasing: true });
        } else {
            this.props.onSetAuthRedirectPath('/checkout');
            this.props.history.push('/auth');
        }
    }

    updatePurchaseState(ingredients) {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey]
            })
            .reduce((sum, el) => {
                return sum + el;
            });
        return sum >= 0;
    }
    purchaseCancelHandler = () => {
        this.setState({ purchasing: false });
    }
    purchaseFinishHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {

        const disabledInfo = { ...this.props.ings };
        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0
        }

        let orderSum = null;
        let burger = this.props.err
            ? <p> "There is an error, ingredients cannot be loaded"</p>
            : <Spinner />;

        if (this.props.ings) {
            burger = <>
                <Burger
                    ingredients={this.props.ings} />
                <BuildControls
                    isAuth={this.props.isAuthenticated}
                    purchasable={this.updatePurchaseState(this.props.ings)}
                    price={this.props.price}
                    disabled={disabledInfo}
                    ingredientAdded={this.props.onAddIngredient}
                    ingredientRemoved={this.props.onDeleteIngredient}
                    placeOrder={this.purchasingHandler} />
            </>;

            orderSum = <OrderSummary
                ingredients={this.props.ings}
                price={this.props.price}
                purchaseCancel={this.purchaseCancelHandler}
                purchaseFinish={this.purchaseFinishHandler}
            />;
        }

        return (
            <>
                {burger}
                <Modal
                    modalClose={this.purchaseCancelHandler}
                    show={this.state.purchasing}>
                    {orderSum}
                </Modal>
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null,
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        err: state.burgerBuilder.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddIngredient: (ingName) => dispatch(actions.addIngredient(ingName)),
        onDeleteIngredient: (ingName) => dispatch(actions.removeIngredient(ingName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit()),
        onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));
