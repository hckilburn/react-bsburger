export const SALAD = 'salad';
export const BACON = 'bacon';
export const CHEESE = 'cheese';
export const MEAT = 'meat'; 

export const PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7
}


