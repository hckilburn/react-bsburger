/* eslint-disable no-fallthrough */
import * as actionsTypes from '../actions/actionTypes';
import * as ingredStuff from '../ingredients';
import { updateObject } from '../../shared/utility';

const initState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
}

const addIngredient = (state, action) => {
    const updatedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] + 1 };
    const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
    const updatedState = {
        building: true,
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice + ingredStuff.PRICES[action.ingredientName],
        error: false
    }
    return updateObject(state, updatedState);
};

const removeIngredient = (state, action) => {
    const updatedIngredient = { [action.ingredientName]: state.ingredients[action.ingredientName] - 1 };
    const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
    const updatedState = {
        building: true,
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice - ingredStuff.PRICES[action.ingredientName],
        error: false
    }
    return updateObject(state, updatedState);
};

const setIngredients = (state, action) => {
    return updateObject(state, {
        // This seems to fix the order problem of salad on bottom 
        // when data direct from the firebase initial data store.
        ingredients: {
            salad: action.ingredients.salad,
            bacon: action.ingredients.bacon,
            cheese: action.ingredients.cheese,
            meat: action.ingredients.meat
        },
        building: false,
        totalPrice: 4,
        error: false
    });
};
const reducer = (state = initState, action) => {

    switch (action.type) {
        case actionsTypes.ADD_INGREDIENT: return addIngredient(state, action);
        case actionsTypes.REMOVE_INGREDIENT: return removeIngredient(state, action);
        case actionsTypes.SET_INGREDIENTS: return setIngredients(state, action);
        case actionsTypes.FETCH_INGREDIENTS_FAILED: return updateObject(state, { error: true });
        default: 
            return state;
    }
};

export default reducer;
