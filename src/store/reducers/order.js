import * as actionsTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initState = {
    orders: [],
    loading: false,
    purchased: false,
    error: false
}

const purchaseBurgerInit = (state, action) => {
    return updateObject(state, { purchased: false });
}
const purchaseBurgerStart = (state, action) => {
    return updateObject(state, { loading: true });
}
const purchaseSuccess = (state, action) => {
    const newOrder = updateObject(action.orderData, { id: action.orderId });
    const newOrders = updateObject(state.orders, newOrder);
    return updateObject(state, { orders: newOrders, purchased: true, loading: false });
}
const purchaseFailed = (state, action) => {
    return updateObject(state, { purchased: false, loading: false });
}

const fetchOrderStart = (state, action) => {
    return updateObject(state, { loading: true });
}
const fetchOrderSuccess = (state, action) => {
    return updateObject(state, { orders: action.orders, loading: false });
}
const fetchOrderFailed = (state, action) => {
    return updateObject(state, { loading: false });
}

const reducer = (state = initState, action) => {

    switch (action.type) {

        /// PURCHASE 
        case actionsTypes.PURCHASE_BURGER_START: return purchaseBurgerStart(state, action);
        case actionsTypes.PURCHASE_BURGER_INIT: return purchaseBurgerInit(state, action);
        case actionsTypes.PURCHASE_BURGER_SUCCESS: return purchaseSuccess(state, action);
        case actionsTypes.PURCHASE_BURGER_FAILED: return purchaseFailed(state, action);

        /// ORDERS 
        case actionsTypes.FETCH_ORDERS_START: return fetchOrderStart(state, action);
        case actionsTypes.FETCH_ORDERS_SUCCESS: return fetchOrderSuccess(state, action);
        case actionsTypes.FETCH_ORDERS_FAILED: return fetchOrderFailed(state, action);
        
        default: return state;
    }
}
export default reducer;