export const ADD_INGREDIENT = 'ADD_INGREDIENT';
export const REMOVE_INGREDIENT = 'REMOVE_INGREDIENT';
export const FETCH_INGREDIENTS_FAILED = 'FETCH_INGREDIENTS_FAILED';
export const SET_INGREDIENTS = 'SET_INGREDIENTS';

// order actions
// export const PURCHASE_BURGER = 'PURCHASE_BURGER'; ... async called and it will not make it to the reducer
export const PURCHASE_BURGER_SUCCESS = 'PURCHASE_BURGER_SUCCESS';
export const PURCHASE_BURGER_FAILED = 'PURCHASE_BURGER_FAIL';
export const PURCHASE_BURGER_START = 'PURCHASE_BURGER_START';
export const PURCHASE_BURGER_INIT = 'PURCHASE_BURGER_INIT';




// export const FETCH_ORDERS = 'FETCH_ORDERS';  ... async called and it will not make it to the reducer
//// export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const SET_ORDERS_INIT = 'SET_ORDERS_INIT';

export const FETCH_ORDERS_START = 'FETCH_ORDERS_START';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
// export const SET_ORDERS = 'SET_ORDERS';
export const FETCH_ORDERS_FAILED = 'FETCH_ORDERS_FAILED';

export const AUTH_START = 'AUTH_START'; 
export const AUTH_SUCCESS = 'AUTH_SUCCESS'; 
export const AUTH_FAILED = 'AUTH_FAILED'; 

export const AUTH_LOGOUT = 'AUTH_LOGOUT'; 
export const SET_AUTH_REDIRECT_PATH = 'SET_AUTH_REDIRECT_PATH'; 