import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        userId: userId
    };
};

export const authFailed = (error) => {
    return {
        type: actionTypes.AUTH_FAILED,
        error: error
    };
};

export const checkAuthTimeout = (expirationTimeout) => {
    const timeoutMS = expirationTimeout * 1000;
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, timeoutMS);

    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

//https://firebase.google.com/docs/reference/rest/auth/ 
// Sections 1) Sign up with email / password
// Sections 2) Sign in with email / password

export const auth = (email, password, isSignup) => {
    return dispatch => {
        dispatch(authStart());

        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        }

        // new user or existing user
        let fireBaseURL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyA6TIJO-fQGVXauH5xsSrGfOrC-UX5F6uw';
        if (isSignup) {
            fireBaseURL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyA6TIJO-fQGVXauH5xsSrGfOrC-UX5F6uw';
        }

        axios.post(fireBaseURL, authData)
            .then(response => {
                // console.log(response);
                const timeNowTime = new Date().getTime();
                const timeTicksTillExpire = response.data.expiresIn * 1000;
                const expirationDateTime = timeTicksTillExpire + timeNowTime;
                const expirationDate = new Date(expirationDateTime);
                // console.log('timeTicksTillExpire  ', timeTicksTillExpire);
                // console.log('timeNowTime          ', timeNowTime);
                // console.log('expirationDateTime   ', expirationDateTime);
                // console.log('rev expirationDate   ', new Date(expirationDateTime));
                // console.log('stored expirationDate', expirationDate);
                // console.log('timeNow              ', new Date(timeNowTime));
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.localId);
                dispatch(authSuccess(response.data.idToken, response.data.localId));
                dispatch(checkAuthTimeout(response.data.expiresIn));
            })
            .catch(err => {
                // console.log(err);
                dispatch(authFailed(err.response.data.error));
            });
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout());
        } else {
            const userId = localStorage.getItem("userId");
            const expirationDate = localStorage.getItem('expirationDate');
            if ( new Date(expirationDate).getTime() > new Date().getTime() ) {
                dispatch(authSuccess(token, userId));
                dispatch(checkAuthTimeout((new Date(expirationDate).getTime() - new Date().getTime()) / 1000 ) );
            } else {
                dispatch(logout());
            }
        };
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};
