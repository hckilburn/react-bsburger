// import React from 'react';
import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

export const purchaseInit = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_INIT
    };
};
export const purchaseBurgerStart = (orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_START
    };
};

export const purchaseBurger = (orderData, token) => {
    return dispatch => {
        dispatch(purchaseBurgerStart());
        axios.post('/orders.json?auth=' + token, orderData)
            .then(res => {
                dispatch(purchaseBurgerSuccess(res.data.name, orderData));
            })
            .catch(res => res, error => {
                dispatch(purchaseBurgerFailed(error));
            })
    };
};

export const purchaseBurgerSuccess = (id, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData: orderData
    };
};
export const purchaseBurgerFailed = (error) => {
    return{ 
        type: actionTypes.PURCHASE_BURGER_FAILED,
        error: error
    };
};

// =========================================


export const fetchOrders = (token, userId) => {
    return dispatch => {
        dispatch(fetchOrdersStart());
        const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"';
        axios.get('/orders.json' + queryParams)
            .then(res => {
                //console.log("hck1", res.data);
                const fetchedOrders = [];
                for (let key in res.data) {
                    fetchedOrders.push({ id: key, ...res.data[key] });
                }
                //console.log("hck2", fetchedOrders);
                //this.setState({ loading: false, orders: fetchedOrders });
                dispatch (fetchOrdersSuccess(fetchedOrders));
            })
            .catch(res => res, error => {
                //this.setState({ loading: false });
                dispatch (fetchOrdersFailed(error));
            })
    };
};
export const fetchOrdersSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders: orders
    };
};
export const fetchOrdersFailed = (error) => {
    return{
        type: actionTypes.FETCH_ORDERS_FAILED,
        error: error
    };
};

export const fetchOrdersStart = () => {
    return{
        type: actionTypes.FETCH_ORDERS_START
    };
};